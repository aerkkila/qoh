void _qoh_luo_vektorit(int tyyppi, const void* y, unsigned pit, QVector<double> sija[2], double x0, double xstep) {
    QVector<double> x1(pit), y1(pit);
    switch(tyyppi) {
	case qoh_i1:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((signed char*)y)[i];
	    }
	    break;
	case qoh_i2:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((signed short*)y)[i];
	    }
	    break;
	case qoh_i4:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((signed int*)y)[i];
	    }
	    break;
	case qoh_i8:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((signed long*)y)[i];
	    }
	    break;
	case qoh_u1:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((unsigned char*)y)[i];
	    }
	    break;
	case qoh_u2:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((unsigned short*)y)[i];
	    }
	    break;
	case qoh_u4:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((unsigned int*)y)[i];
	    }
	    break;
	case qoh_u8:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((unsigned long*)y)[i];
	    }
	    break;
	case qoh_f4:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((float*)y)[i];
	    }
	    break;
	case qoh_f8:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((double*)y)[i];
	    }
	    break;
	case qoh_f10:
	    for(unsigned i=0; i<pit; i++) {
		x1[i] = x0 + i*xstep;
		y1[i] = ((long double*)y)[i];
	    }
	    break;
	default:
	    puts("tuntematon tyyppi");
	    return;
    }
    sija[0] = y1;
    sija[1] = x1;
}

QVector<double> _qoh_luo_vektori(int tyyppi, const void* y, unsigned pit) {
    QVector<double> y1(pit);
    switch(tyyppi) {
	case qoh_i1:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((signed char*)y)[i];
	    break;
	case qoh_i2:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((signed short*)y)[i];
	    break;
	case qoh_i4:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((signed int*)y)[i];
	    break;
	case qoh_i8:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((signed long*)y)[i];
	    break;
	case qoh_u1:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((unsigned char*)y)[i];
	    break;
	case qoh_u2:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((unsigned short*)y)[i];
	    break;
	case qoh_u4:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((unsigned int*)y)[i];
	    break;
	case qoh_u8:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((unsigned long*)y)[i];
	    break;
	case qoh_f4:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((float*)y)[i];
	    break;
	case qoh_f8:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((double*)y)[i];
	    break;
	case qoh_f10:
	    for(unsigned i=0; i<pit; i++)
		y1[i] = ((long double*)y)[i];
	    break;
	default:
	    puts("tuntematon tyyppi");
	    return y1;
    }
    return y1;
}

#if 0
void _qoh_täytä_cmapdata(int tyyppi, const void* dt, unsigned ypit, unsigned xpit, QCPColorMapData* data) {
    switch(tyyppi) {
	case qoh_i1:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((signed char*)dt)[j*xpit+i]);
	    break;
	case qoh_i2:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((signed short*)dt)[j*xpit+i]);
	    break;
	case qoh_i4:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((signed int*)dt)[j*xpit+i]);
	    break;
	case qoh_i8:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((signed long*)dt)[j*xpit+i]);
	    break;
	case qoh_u1:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((unsigned char*)dt)[j*xpit+i]);
	    break;
	case qoh_u2:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((unsigned short*)dt)[j*xpit+i]);
	    break;
	case qoh_u4:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((unsigned int*)dt)[j*xpit+i]);
	    break;
	case qoh_u8:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((unsigned long*)dt)[j*xpit+i]);
	    break;
	case qoh_f4:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((float*)dt)[j*xpit+i]);
	    break;
	case qoh_f8:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((double*)dt)[j*xpit+i]);
	    break;
	case qoh_f10:
	    for(unsigned j=0; j<ypit; j++)
		for(unsigned i=0; i<xpit; i++)
		    data->setCell(i, j, ((long double*)dt)[j*xpit+i]);
	    break;
	default:
	    puts("tuntematon tyyppi");
    }
}
#endif
