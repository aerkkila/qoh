#include <qcustomplot.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <sys/wait.h>
#include <math.h>
#define __qoh_internal__
#include "qoh.h"
#undef __qoh_internal__
#include "axis_ticker_log.cc"

#ifdef COLORMAP_HEADERS
#include <cmh_colormaps.h>
#endif

#define _min(a,b) ((a) < (b) ? a : (b))
#define _max(a,b) ((a) > (b) ? a : (b))
#define Printf(...) do if (qoh_verbose) printf(__VA_ARGS__); while(0)
#define Free(a) (a) = (free(a), (typeof(a))0)

int qoh_png_w = 600,
    qoh_png_h = 400,
    qoh_png_laatu = -1;
float qoh_fontsize = -1,
      qoh_xmargin = 60;
double qoh_png_scale = 2;
unsigned char qoh_sorted, qoh_verbose = 0;
char qoh_x2=0, qoh_y2=0;
const unsigned char qoh_värit[][3] = {{0,0,255},{255,0,0},{0,205,80},{0,0,0},{150,100,0}};
const unsigned char qoh_oletusvärejä = sizeof(qoh_värit)/sizeof(qoh_värit[0]);
unsigned char qoh_scattersize = 7;
int qoh_legend_ntries = 4;
int qoh_legend_addw = 0,
    qoh_legend_addh = 0;

static QCPColorGradient last_cmap;
static double last_cmap_min, last_cmap_max, last_cmap_mid;

/* The same group gets the same color in set_colors_from. */
#define max_colorgroups 256 // cannot be larger
static short *colorgroups;
static int capacity_of_colorgroups;
static int qoh_ncolors;
static char used_colorgroups[max_colorgroups] = {0};
short qoh_colorgroup = qoh_nogroup;

int qoh_sij[] = {
    [0]			= 0,
    [_qoh_left]		= Qt::AlignLeft,
    [_qoh_right]	= Qt::AlignRight,
    [_qoh_top]		= Qt::AlignTop,
    [_qoh_bottom]	= Qt::AlignBottom,
};

#include "liitettävää.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wwrite-strings"
int _argc = 1;
char* _argv[] = {"-"};
QApplication app(_argc, _argv);
#pragma GCC diagnostic pop

static void _set_axestickfont();

enum piirtotapa {sironta_e, suora_e};

struct Ikkuna: public QWidget {
    Ikkuna() {
	this->init();
    }
    ~Ikkuna() {
	this->vapauta();
    }
    void init() {
	memset(this->x_used, 0, sizeof(this->x_used));
	memset(this->y_used, 0, sizeof(this->y_used));
	delete this->plot;
	this->plot = new QCustomPlot();
	qoh_ticks_outside();
    }
    void vapauta() {
	delete this->plot; this->plot = NULL;
	Free(colorgroups);
	capacity_of_colorgroups = 0;
	memset(used_colorgroups, 0, max_colorgroups*sizeof(*used_colorgroups));
	qoh_ncolors = 0;
    }
    void piirrä(QVector<double>, QVector<double>, enum piirtotapa);
    QCPAxis* gca_x() {
	return qoh_x2 ? this->plot->xAxis2 : this->plot->xAxis;
    }
    QCPAxis* gca_y() {
	return qoh_y2 ? this->plot->yAxis2 : this->plot->yAxis;
    }
    //void paintEvent(QPaintEvent*);
    QCustomPlot* plot;
    QFont font;
    QCPRange x[2], y[2]; // datan skaala, kuvan skaala laitetaan vähän suuremmaksi
    char x_used[2], y_used[2];
};

struct Ikkuna ikk;

/* plot_yz piirtää kaikki eri värit omina graafeinaan.
   Kuitenkin käyttäjän näkökulmasta tämä on vain yksi graafi. */
int eilaskettavia_graafeja = 0;

static void tee_ymarginaali() {
    int ind = !!qoh_y2;
    QCPAxis* yax = ikk.gca_y();
    double marginaali = ikk.y[ind].size() / 60;
    QCPRange m = ikk.y[ind];
    m.lower -= marginaali;
    m.upper += marginaali;
    yax->setRange(m);
}

void Ikkuna::piirrä(QVector<double> x, QVector<double> y, enum piirtotapa tapa) {
    QMargins marg;
    ikk.plot->axisRect()->insetLayout()->setMargins(marg);
    QCPAxis* xax = this->gca_x();
    QCPAxis* yax = this->gca_y();
    xax->setVisible(1);
    yax->setVisible(1);
    QCPGraph* graph = this->plot->addGraph(xax, yax);
    graph->setData(x, y, qoh_sorted);
    qoh_sorted = 0;
    switch(tapa) {
	case sironta_e:
	    graph->setLineStyle(QCPGraph::lsNone);
	    graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, qoh_scattersize));
	    break;
	case suora_e:
	    break;
    }
    graph->rescaleAxes();
    ikk.plot->legend->item(this->plot->graphCount()-1)->setVisible(0);
    QPen pen = graph->pen();
    int count = qoh_graphcount();
    const unsigned char* v = qoh_värit[count%qoh_oletusvärejä];
    pen.setColor(QColor(v[0], v[1], v[2]));
    graph->setPen(pen);

    /* colorgroup */
    if (capacity_of_colorgroups < qoh_graphcount())
	colorgroups = (typeof(colorgroups))realloc(colorgroups, (capacity_of_colorgroups=count+10)*sizeof(*colorgroups));
    qoh_colorgroup %= max_colorgroups;
    colorgroups[count-1] = qoh_colorgroup;
    if (qoh_colorgroup == qoh_nogroup)
	qoh_ncolors++;
    else {
	qoh_ncolors += !used_colorgroups[qoh_colorgroup];
	used_colorgroups[qoh_colorgroup] = 1;
    }

    bool found;
    QCPRange range, m;
    double marginaali;
    int ind;

    /* x */
    range = graph->getKeyRange(found);
    ind = !!qoh_x2;
    if (!x_used[ind])
	this->x[ind] = range;
    else
	this->x[ind].expand(range);
    marginaali = this->x[ind].size() / qoh_xmargin;
    m = this->x[ind];
    m.lower -= marginaali;
    m.upper += marginaali;
    xax->setRange(m);
    x_used[ind] = 1;

    /* y */
    range = graph->getValueRange(found);
    ind = !!qoh_y2;
    if (!y_used[ind])
	this->y[ind] = range;
    else
	this->y[ind].expand(range);
    tee_ymarginaali();
    y_used[ind] = 1;

    _set_axestickfont();
}

static void _set_axestickfont() {
    ikk.plot->xAxis->setTickLabelFont(ikk.font);
    ikk.plot->xAxis2->setTickLabelFont(ikk.font);
    ikk.plot->yAxis->setTickLabelFont(ikk.font);
    ikk.plot->yAxis2->setTickLabelFont(ikk.font);
}

int qoh_ngraphs() {
    return ikk.plot->graphCount();
}

int qoh_graphcount() {
    return ikk.plot->graphCount() - eilaskettavia_graafeja;
}

void qoh_clf() {
    ikk.plot->clearPlottables();
    eilaskettavia_graafeja = 0;
    ikk.vapauta();
    ikk.init();
}

void _qoh_dashstyle(float line, float space, int taakse) {
    QCPGraph* graph = ikk.plot->graph(ikk.plot->graphCount()-1-taakse);
    graph->setLineStyle(QCPGraph::lsLine);
    QPen pen = graph->pen();
    QVector<double> list;
    list << line << space;
    pen.setDashPattern(list);
    graph->setPen(pen);
}

void _qoh_linestyle(const char* str, int taakse) {
    QCPGraph* graph = ikk.plot->graph(ikk.plot->graphCount()-1-taakse);
    QPen pen = graph->pen();
    if ('0' <= str[0] && str[0] <= '9') {
	pen.setWidth(str[0] - '0');
	str++;
    }
    if (str[0] == '\0') { graph->setLineStyle(QCPGraph::lsNone); return; }
    graph->setLineStyle(QCPGraph::lsLine);
    switch((str[0]<<8) + str[1]) {
	case ('-'<<8):       pen.setStyle(Qt::SolidLine);   break;
	case ('-'<<8) + '-': pen.setStyle(Qt::DashLine);    break;
	case ('.'<<8) + '.': pen.setStyle(Qt::DotLine);     break;
	case ('-'<<8) + '.': pen.setStyle(Qt::DashDotLine); break;
    }
    graph->setPen(pen);
}

#define A(tyyli) graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::tyyli, qoh_scattersize))
void _qoh_scatterstyle(int merkki, int taakse) {
    QCPGraph* graph = ikk.plot->graph(ikk.plot->graphCount()-1-taakse);
    switch(merkki) {
	case ' ': A(ssNone); break;
	case '.': A(ssDot); break;
	case 'x': A(ssCross); break;
	case '+': A(ssPlus); break;
	case 'O': A(ssCircle); break;
	case 'o': A(ssDisc); break;
	case '*': A(ssStar); break;
	case '<': A(ssTriangle); break;
	default: A(ssCrossSquare); break;
    }
}
#undef A

void qoh_color_rgb(unsigned char r, unsigned char g, unsigned char b, int taakse) {
    QCPGraph* graph = ikk.plot->graph(ikk.plot->graphCount()-1-taakse);
    QPen pen = graph->pen();
    pen.setColor(QColor(r, g, b));
    graph->setPen(pen);
}

void qoh_color_ptr(unsigned char *rgb, int taakse) {
    qoh_color_rgb(rgb[0], rgb[1], rgb[2], taakse);
}

void qoh_set_fontsize(float size) {
    qoh_fontsize = size;
    ikk.font = ikk.gca_x()->tickLabelFont();
    ikk.font.setPointSizeF(qoh_fontsize);
    _set_axestickfont();
}

void qoh_set_colors_from(int cmh_enum) {
    float interval = 256.0 / qoh_ncolors;
    int total = ikk.plot->graphCount();
    short group_irgb[max_colorgroups];
    memset(group_irgb, -1, sizeof(group_irgb));
    int icolor = 0;
    for (int i=0; i<total; i++) {
	int icolor_now, group = colorgroups[total-1-i];
	switch (group) {
	    case qoh_ignoregroup:
		continue;
	    case qoh_nogroup:
		icolor_now = icolor++;
		break;
	    default:
		if (group_irgb[group]<0)
		    group_irgb[group] = icolor_now = icolor++;
		else
		    icolor_now = group_irgb[group];
		break;
	}
	unsigned char* rgb = cmh_colorvalue(cmh_enum, (int)round((icolor_now+0.5)*interval));
	qoh_color_ptr(rgb, i);
    }
}

void qoh_color_default_n(int n) {
    QCPGraph* graph = ikk.plot->graph(ikk.plot->graphCount()-1);
    QPen pen = graph->pen();
    const unsigned char* v = qoh_värit[n % qoh_oletusvärejä];
    pen.setColor(QColor(v[0], v[1], v[2]));
    graph->setPen(pen);
}

#if 0
void qoh_plot_args(struct qoh_args *args) {
    Qvector<double> yxz[3] = {0};
    if (args->x) {
	yxz[0] = _qoh_luo_vektori(args->ytype, args->y, args->N);
	yxz[1] = _qoh_luo_vektori(args->xtype, args->x, args->N);
    }
    else {
	double xstep = args->xstep ? args->xstep : 1;
	_qoh_luo_vektorit(args->ytyyppi, args->y, args->N, yxz, args->x0, xstep);
    }
    if (args->z)
	yxz[2] = _qoh_luo_vektori(args->ztype, args->z, args->N);
}
#endif

void _qoh_plot_y(int tyyppi, const void* y, unsigned pit, double x0, double xstep) {
    QVector<double> yx[2];
    _qoh_luo_vektorit(tyyppi, y, pit, yx, x0, xstep);
    qoh_sorted = xstep >= 0;
    ikk.piirrä(yx[1], yx[0], sironta_e);
}

void _qoh_plot_yx(int ytyyppi, int xtyyppi, const void* y, const void* x, unsigned pit) {
    QVector<double> yx[2];
    yx[0] = _qoh_luo_vektori(ytyyppi, y, pit);
    yx[1] = _qoh_luo_vektori(xtyyppi, x, pit);
    ikk.piirrä(yx[1], yx[0], sironta_e);
}

/* Varoitus: ikk.plot->graphCount() kasvaa enemmän kuin yhdellä, vaikka piirretään vain yksi asia. */
void _qoh_plot_yz(int ytyyppi, int ztyyppi, const void* datay, const void* dataz, unsigned pit, int icmap, double* norm, double x0, double xstep) {
    QVector<double> yx[2];
    _qoh_luo_vektorit(ytyyppi, datay, pit, yx, x0, xstep);
    qoh_sorted = xstep >= 0;
    int invert = 0;
    if (icmap < 0) {
	invert = 1;
	icmap = -icmap;
    }
    last_cmap.clearColorStops();

    double _norm[3] = {NAN, NAN, NAN};
    if (!norm)
	norm = _norm;
    
    QVector<double> zvec = _qoh_luo_vektori(ztyyppi, dataz, pit);
    double zmin = norm[0]==norm[0] ? norm[0] : *std::min_element(zvec.constBegin(), zvec.constEnd());
    double zmax = norm[2]==norm[2] ? norm[2] : *std::max_element(zvec.constBegin(), zvec.constEnd());
    double zmid = norm[1]==norm[1] ? norm[1] : (zmin+zmax) / 2;
    last_cmap_min = zmin;
    last_cmap_max = zmax;
    last_cmap_mid = zmid;

    double min = zmin, max = zmid;
    int c0=0, c1=128;
    double cmap_coeff = zmid / (zmax-zmin);
    double cmap0 = 0;
    int wasgroup = qoh_colorgroup;
    qoh_colorgroup = qoh_ignoregroup;
    for(int twice=0; twice<2; twice++) {
	double space = (max - min) / 128;
	double r0 = min;
	for(int c=c0; c<c1; c++) {
	    double r1 = min + (c-c0+1) * space;
#ifdef COLORMAP_HEADERS
	    unsigned char* väri = cmh_colorvalue(icmap, invert ? 255-c : c);
#else
	    char v = invert ? 255-c : c;
	    unsigned char väri[] = {v,v,v};
#endif

	    QColor qväri(väri[0], väri[1], väri[2]);
	    double cmap_at = (c-c0+0.5)/128 * cmap_coeff + cmap0;
	    last_cmap.setColorStopAt(cmap_at, qväri);

	    if (c == 255)
		r1 = 1e50;
	    else if (c == 0)
		r0 = -1e50;
	    QVector<double> y1, x1;
	    for(unsigned i=0; i<pit; i++)
		if (r0 <= zvec[i] && zvec[i] < r1) {
		    y1.append(yx[0][i]);
		    x1.append(yx[1][i]);
		}
	    if (y1.size()) {
		ikk.piirrä(x1, y1, sironta_e);
		qoh_color_rgb(väri[0], väri[1], väri[2], 0);
		eilaskettavia_graafeja++;
	    }
	    r0 = r1;
	}
	min = zmid;
	max = zmax;
	cmap0 = cmap_coeff;
	cmap_coeff = 1-cmap_coeff;
	c0 = 128;
	c1 = 256;
    }
    qoh_colorgroup = wasgroup;
}

void qoh_colorbar(const char* title, int place) {
    auto scale = new QCPColorScale(ikk.plot);
    QCPRange range(last_cmap_min, last_cmap_max);
    scale->setDataRange(range);
    scale->setGradient(last_cmap);
    int col = place==qoh_left ? 0 : ikk.plot->plotLayout()->columnCount();
    int row = place==qoh_top  ? 0 : ikk.plot->plotLayout()->rowCount();
    if (place == qoh_bottom || place == qoh_top) {
	ikk.plot->plotLayout()->insertRow(row);
	col--;
	scale->setType(place==qoh_bottom ? QCPAxis::atBottom : QCPAxis::atTop);
    }
    else {
	ikk.plot->plotLayout()->insertColumn(col);
	row--;
	scale->setType(place==qoh_left ? QCPAxis::atLeft : QCPAxis::atRight);
    }
    ikk.plot->plotLayout()->addElement(row, col, scale);
    if (title)
	scale->axis()->setLabel(title);
}

void qoh_line(double y0, double x0, double y1, double x1) {
    QVector<double> y(2), x(2);
    y[0] = y0; x[0] = x0;
    y[1] = y1; x[1] = x1;
    ikk.piirrä(x, y, suora_e);
}

void qoh_title(const char* str) {
    const QString qstr(str);
    QCPTextElement *title = new QCPTextElement(ikk.plot);
    title->setText(str);
    title->setFont(QFont("sans", 12, QFont::Medium));
    ikk.plot->plotLayout()->insertRow(0);
    ikk.plot->plotLayout()->addElement(0, 0, title);
}

void _qoh_label(const char* str, int taakse) {
    ikk.plot->legend->item(ikk.plot->graphCount()-1-taakse)->setVisible(!!str);
    ikk.plot->graph(ikk.plot->graphCount()-1-taakse)->setName(str);
}

void qoh_xlabel(const char* str) {
    QCPAxis* ax = ikk.gca_x();
    ax->setLabel(str);
    ax->setLabelFont(ikk.font);
}

void qoh_ylabel(const char* str) {
    QCPAxis* ax = ikk.gca_y();
    ax->setLabel(str);
    ax->setLabelFont(ikk.font);
}

void qoh_xlabel1(const char* str) {
    QCPAxis* ax = ikk.plot->xAxis;
    ax->setVisible(1);
    ax->setLabel(str);
    ax->setLabelFont(ikk.font);
}

void qoh_ylabel1(const char* str) {
    QCPAxis* ax = ikk.plot->yAxis;
    ax->setVisible(1);
    ax->setLabel(str);
    ax->setLabelFont(ikk.font);
}

void qoh_xlabel2(const char* str) {
    QCPAxis* ax = ikk.plot->xAxis2;
    ax->setVisible(1);
    ax->setLabel(str);
    ax->setLabelFont(ikk.font);
}

void qoh_ylabel2(const char* str) {
    QCPAxis* ax = ikk.plot->yAxis2;
    ax->setVisible(1);
    ax->setLabel(str);
    ax->setLabelFont(ikk.font);
}

void qoh_ymax(double value) {
    if (qoh_y2)
	ikk.plot->yAxis2->setRangeUpper(value);
    else
	ikk.plot->yAxis->setRangeUpper(value);
}

void qoh_ymin(double value) {
    if (qoh_y2)
	ikk.plot->yAxis2->setRangeLower(value);
    else
	ikk.plot->yAxis->setRangeLower(value);
}

void qoh_xmax(double value) {
    if (qoh_x2)
	ikk.plot->xAxis2->setRangeUpper(value);
    else
	ikk.plot->xAxis->setRangeUpper(value);
}

void qoh_xmin(double value) {
    if (qoh_x2)
	ikk.plot->xAxis2->setRangeLower(value);
    else
	ikk.plot->xAxis->setRangeLower(value);
}

double qoh_get_lower(int where) {
    switch(where) {
	case qoh_left:
	    return ikk.plot->yAxis->range().lower;
	case qoh_bottom:
	    return ikk.plot->xAxis->range().lower;
	case qoh_right:
	    return ikk.plot->yAxis2->range().lower;
	case qoh_top:
	    return ikk.plot->xAxis2->range().lower;
    }
    return 0.0/0.0;
}

double qoh_get_upper(int where) {
    switch(where) {
	case qoh_left:
	    return ikk.plot->yAxis->range().upper;
	case qoh_bottom:
	    return ikk.plot->xAxis->range().upper;
	case qoh_right:
	    return ikk.plot->yAxis2->range().upper;
	case qoh_top:
	    return ikk.plot->xAxis2->range().upper;
    }
    return 0.0/0.0;
}

void qoh_xaxis_log(double base) {
    ikk.gca_x()->setScaleType(QCPAxis::stLogarithmic);
    qoh_xticker_log(base);
}

void qoh_yaxis_log(double base) {
    ikk.gca_y()->setScaleType(QCPAxis::stLogarithmic);
    qoh_yticker_log(base);
}

void _qoh_xticker_log(double base, int tickcount) {
    QSharedPointer<QohAxisTickerLog> ticker(new QohAxisTickerLog);
    ikk.gca_x()->setTicker(ticker);
    if (base > 0)
	ticker->setLogBase(base);
    if (tickcount > 0)
	ticker->setTickCount(tickcount);
}

void _qoh_yticker_log(double base, int tickcount) {
    QSharedPointer<QohAxisTickerLog> ticker(new QohAxisTickerLog);
    ikk.gca_y()->setTicker(ticker);
    if (base > 0)
	ticker->setLogBase(base);
    if (tickcount > 0)
	ticker->setTickCount(tickcount);
}

void _qoh_xticker_datetime(const char* format, int tickcount) {
    QSharedPointer<QCPAxisTickerDateTime> ticker(new  QCPAxisTickerDateTime);
    ikk.gca_x()->setTicker(ticker);
    ticker->setDateTimeFormat(format);
    if (tickcount > 1)
	ticker->setTickCount(tickcount);
}

void _qoh_yticker_datetime(const char* format, int tickcount) {
    QSharedPointer<QCPAxisTickerDateTime> ticker(new  QCPAxisTickerDateTime);
    ikk.gca_y()->setTicker(ticker);
    ticker->setDateTimeFormat(format);
    if (tickcount > 1)
	ticker->setTickCount(tickcount);
}

void _qoh_xticker1_datetime(const char* format, int tickcount) {
    QSharedPointer<QCPAxisTickerDateTime> ticker(new  QCPAxisTickerDateTime);
    ikk.plot->xAxis->setTicker(ticker);
    ticker->setDateTimeFormat(format);
    if (tickcount > 1)
	ticker->setTickCount(tickcount);
}

void _qoh_xticker2_datetime(const char* format, int tickcount) {
    QSharedPointer<QCPAxisTickerDateTime> ticker(new  QCPAxisTickerDateTime);
    ikk.plot->xAxis2->setTicker(ticker);
    ikk.plot->xAxis2->setVisible(1);
    ticker->setDateTimeFormat(format);
    if (tickcount > 1)
	ticker->setTickCount(tickcount);
}

void _qoh_yticker1_datetime(const char* format, int tickcount) {
    QSharedPointer<QCPAxisTickerDateTime> ticker(new  QCPAxisTickerDateTime);
    ikk.plot->yAxis->setTicker(ticker);
    ticker->setDateTimeFormat(format);
    if (tickcount > 1)
	ticker->setTickCount(tickcount);
}

void _qoh_yticker2_datetime(const char* format, int tickcount) {
    QSharedPointer<QCPAxisTickerDateTime> ticker(new  QCPAxisTickerDateTime);
    ikk.plot->yAxis2->setTicker(ticker);
    ikk.plot->yAxis2->setVisible(1);
    ticker->setDateTimeFormat(format);
    if (tickcount > 1)
	ticker->setTickCount(tickcount);
}

void qoh_set_range(int where, double lower, double upper) {
    switch(where) {
	case qoh_left:
	    ikk.plot->yAxis->setRange(QCPRange(lower, upper));
	    break;
	case qoh_bottom:
	    ikk.plot->xAxis->setRange(QCPRange(lower, upper));
	    break;
	case qoh_right:
	    ikk.plot->yAxis2->setRange(QCPRange(lower, upper));
	    break;
	case qoh_top:
	    ikk.plot->xAxis2->setRange(QCPRange(lower, upper));
	    break;
    }
}

void* qoh_init_ticker(const char *axisname) {
    QCPAxis *axis;
    if (axisname[0] == 'y') {
	if (axisname[1] == '2')
	    axis = ikk.plot->yAxis2;
	else
	    axis = ikk.plot->yAxis;
    }
    else {
	if (axisname[1] == '2')
	    axis = ikk.plot->xAxis2;
	else
	    axis = ikk.plot->xAxis;
    }
  QSharedPointer<QCPAxisTickerText> ticker(new QCPAxisTickerText);
  axis->setTicker(ticker);
  return &*ticker;
}

void qoh_set_tick(void *vticker, float position, const char *label) {
    auto ticker = (QCPAxisTickerText*)vticker;
    ticker->addTick(position, label);
}

void qoh_set_visible(int where, int vis) {
    switch(where) {
	case qoh_left:
	    ikk.plot->yAxis->setVisible(vis);
	    break;
	case qoh_bottom:
	    ikk.plot->xAxis->setVisible(vis);
	    break;
	case qoh_right:
	    ikk.plot->yAxis2->setVisible(vis);
	    break;
	case qoh_top:
	    ikk.plot->xAxis2->setVisible(vis);
	    break;
    }
}

void qoh_set_grid_visible(int where, int vis) {
    switch(where) {
	case qoh_left:
	    ikk.plot->yAxis->grid()->setVisible(vis);
	    break;
	case qoh_bottom:
	    ikk.plot->xAxis->grid()->setVisible(vis);
	    break;
	case qoh_right:
	    ikk.plot->yAxis2->grid()->setVisible(vis);
	    break;
	case qoh_top:
	    ikk.plot->xAxis2->grid()->setVisible(vis);
	    break;
    }
}

static QCPAxisTickerFixed::ScaleStrategy _scalemod[] = {
    [qoh_scalemod_none] = QCPAxisTickerFixed::ssNone,
    [qoh_scalemod_mult] = QCPAxisTickerFixed::ssMultiples,
    [qoh_scalemod_powr] = QCPAxisTickerFixed::ssPowers,
};

void qoh_ticker_fixed(int xy, double step, int scalemod) {
    QSharedPointer<QCPAxisTickerFixed> ticker(new QCPAxisTickerFixed);
    int joo = 0;
    if (xy == 'x' || xy == 0 || xy == 'x'+'y' || xy == 2)
	ikk.plot->xAxis->setTicker(ticker), joo = 1;
    if (xy == 'y' || xy == 1 || xy == 'x'+'y' || xy == 2)
	ikk.plot->yAxis->setTicker(ticker), joo = 1;
    if (!joo)
	return;
    ticker->setTickStep(step);
    ticker->setScaleStrategy(_scalemod[scalemod]);
}

void qoh_ticklabels_inside() {
    ikk.plot->xAxis->setTickLabelSide(QCPAxis::lsInside);
    ikk.plot->xAxis2->setTickLabelSide(QCPAxis::lsInside);
    ikk.plot->yAxis->setTickLabelSide(QCPAxis::lsInside);
    ikk.plot->yAxis2->setTickLabelSide(QCPAxis::lsInside);
}

void qoh_ticklabels_outside() {
    ikk.plot->xAxis->setTickLabelSide(QCPAxis::lsOutside);
    ikk.plot->xAxis2->setTickLabelSide(QCPAxis::lsOutside);
    ikk.plot->yAxis->setTickLabelSide(QCPAxis::lsOutside);
    ikk.plot->yAxis2->setTickLabelSide(QCPAxis::lsOutside);
}

static void _ticks_in(QCPAxis* ax) {
    int length = ax->tickLengthOut();
    if (length)
	ax->setTickLength(length, 0);
    length = ax->subTickLengthOut();
    if (length)
	ax->setSubTickLength(length, 0);
}

static void _ticks_out(QCPAxis* ax) {
    int length = ax->tickLengthIn();
    if (length)
	ax->setTickLength(0, length);
    length = ax->subTickLengthIn();
    if (length)
	ax->setSubTickLength(0, length);
}

void qoh_ticks_inside() {
    _ticks_in(ikk.plot->xAxis);
    _ticks_in(ikk.plot->yAxis);
    _ticks_in(ikk.plot->xAxis2);
    _ticks_in(ikk.plot->yAxis2);
}

void qoh_ticks_outside() {
    _ticks_out(ikk.plot->xAxis);
    _ticks_out(ikk.plot->yAxis);
    _ticks_out(ikk.plot->xAxis2);
    _ticks_out(ikk.plot->yAxis2);
}

void _qoh_linewidth(float w, int taakse) {
    QCPGraph* graph = ikk.plot->graph(ikk.plot->graphCount()-1-taakse);
    QPen pen = graph->pen();
    pen.setWidth(w);
    graph->setPen(pen);
}

static inline int my_isnan_float(float f) {
    const uint32_t exponent = ((1u<<31)-1) - ((1u<<(31-8))-1);
    uint32_t bits;
    memcpy(&bits, &f, 4);
    return (bits & exponent) == exponent;
}

static int paras_legend() {
    int x,y,legw,legh, axisw, axish;
    enum {nurkka_e, pysty_e, vaaka_e, sisä_e};
    ikk.plot->setViewport(QRect(0, 0, qoh_png_w, qoh_png_h));
    ikk.plot->replot();
    axisw = ikk.plot->axisRect()->width();
    axish = ikk.plot->axisRect()->height();
    QRect rect = ikk.plot->legend->outerRect();
    rect.getRect(&x,&y,&legw,&legh); // x ja y jäävät turhiksi
    legw += qoh_legend_addw;
    legh += qoh_legend_addh;
    int graafeja = ikk.plot->graphCount();
    int kierros = 0;
#define max_zeros 12
    int paras_sij[max_zeros][4] = {0};
    int paras = (1u<<31)-1;

    float vasen0, oikea0, ylä0, ala0;
    {
	QRect r = ikk.plot->axisRect()->rect();
	vasen0 = r.left();
	oikea0 = r.right();
	ylä0 = r.top();
	ala0 = r.bottom();
    }
    int nurkkalaskenta[2][2] = {0};

    int laskenta[qoh_legend_ntries][axish/legh][axisw/legw];
    memset(laskenta,  0, sizeof(laskenta));
    int nrows_arr[qoh_legend_ntries];
    int ncols_arr[qoh_legend_ntries];

#define Ysiirto(kierros) (legh * kierros / qoh_legend_ntries)
#define Xsiirto(kierros) (legw * kierros / qoh_legend_ntries)
while_kierros_lessthan_qoh_legend_ntries:
    int current_zeros = 0;
    int ysiirto = Ysiirto(kierros),
	xsiirto = Xsiirto(kierros);
    int nrows = (axish - ysiirto) / legh,
	ncols = (axisw - xsiirto) / legw;
    nrows_arr[kierros] = nrows;
    ncols_arr[kierros] = ncols;
    int legend_pino_h = nrows * legh,
	legend_pino_w = ncols * legw,
	xlaskenta[2][ncols],
	ylaskenta[2][nrows];
    memset(xlaskenta, 0, sizeof(xlaskenta)); // ala- tai yläreunan sijainnit
    memset(ylaskenta, 0, sizeof(ylaskenta)); // oikeat ja vasemmat sijainnit

    /* Lasketaan, montako pistettä on kullakin alueella. */
    for(int g=0; g<graafeja; g++) {
	QCPGraph* graafi = ikk.plot->graph(g);
	int N = graafi->dataCount();
	for(int p=0; p<N; p++) {
	    QPointF piste = graafi->dataPixelPosition(p);
	    float x = piste.x();
	    float y = piste.y();
	    if (x < 0 || y < 0 || my_isnan_float(x) || my_isnan_float(y))
		continue;

	    /* floor because int rounds toward zero which is wrong with negative numbers */
	    int row	= floor((float)(y - ylä0   - ysiirto) / legend_pino_h * nrows);
	    int col	= floor((float)(x - vasen0 - xsiirto) / legend_pino_w * ncols);
	    int goodrow = 0 <= row && row < nrows;
	    int goodcol = 0 <= col && col < ncols;
	    if (goodrow && goodcol)
		laskenta[kierros][row][col]++;

	    /* Tarkistetaan, onko piste x- tai y-suunnassa jommallakummalla reunalla.
	       Tässä käsitellään vain reunapisteitä. */
	    int sarakelaita	= (vasen0+legw <= x) + (oikea0-legw < x); // 0, 1, tai 2: vasen, keskellä, oikea
	    int rivilaita	= (ylä0  +legh <= y) + (ala0  -legh < y); // 0, 1, tai 2: ylä,   keskellä, ala
	    if (sarakelaita != 1) {
		if ((rivilaita != 1) & !kierros)
		    nurkkalaskenta[!!rivilaita][!!sarakelaita]++;
		if (goodrow)
		    ylaskenta[!!sarakelaita][row]++;
	    }
	    if (rivilaita != 1 && goodcol)
		xlaskenta[!!rivilaita][col]++;
	}
    }

    /* Ensimmäisellä kierroksella tarkistetaan nurkkasijainnit. */
    if (kierros == 0)
	for(int j=0; j<2; j++)
	    for(int i=0; i<2; i++)
		if (!nurkkalaskenta[j][i] || nurkkalaskenta[j][i] < paras) {
		    paras = nurkkalaskenta[j][i];
		    paras_sij[current_zeros][0] = nurkka_e;
		    paras_sij[current_zeros][1] = j;
		    paras_sij[current_zeros][2] = i;
		    if (paras == 0 && ++current_zeros >= max_zeros)
			goto valmis; // cannot happen
		}

    /* Sijainnit vasemmalla ja oikealla. */
    for(int i=0; i<2; i++)
	for(int j=0; j<nrows; j++)
	    if (!ylaskenta[i][j] || ylaskenta[i][j] < paras) {
		paras = ylaskenta[i][j];
		paras_sij[current_zeros][0] = pysty_e;
		paras_sij[current_zeros][1] = i;
		paras_sij[current_zeros][2] = j;
		paras_sij[current_zeros][3] = kierros;
		if (paras == 0 && ++current_zeros >= max_zeros)
		    goto valmis;
	    }

    /* Sijainnit ylhäällä ja alhaalla. */
    for(int j=0; j<2; j++)
	for(int i=0; i<ncols; i++)
	    if (!xlaskenta[j][i] || xlaskenta[j][i] < paras) {
		paras = xlaskenta[j][i];
		paras_sij[current_zeros][0] = vaaka_e;
		paras_sij[current_zeros][1] = j;
		paras_sij[current_zeros][2] = i;
		paras_sij[current_zeros][3] = kierros;
		if (paras == 0 && ++current_zeros >= max_zeros)
		    goto valmis;
	    }

    if (qoh_verbose) {
	printf("reunat:\n\t");
	for(int i=0; i<ncols; i++)
	    printf("%7i ", xlaskenta[0][i]);
	printf(" (vaakarivi)\n\t");
	for(int j=0; j<nrows; j++) {
	    printf("%7i ", ylaskenta[0][j]);
	    for(int k=0; k<(ncols-2)*8; k++)
		putchar(' ');
	    printf("%7i\n\t", ylaskenta[1][j]);
	}
	for(int i=0; i<ncols; i++)
	    printf("%7i ", xlaskenta[1][i]);
	printf(" (vaakarivi)\n");

	printf("kokonaan: (%i, %i)\n", nrows, ncols);
	for(int j=0; j<nrows; j++) {
	    putchar('\t');
	    for(int i=0; i<ncols; i++)
		printf("%7i ", laskenta[kierros][j][i]);
	    putchar('\n');
	}
    }

    if (paras && ++kierros < qoh_legend_ntries)
	goto while_kierros_lessthan_qoh_legend_ntries;

    if (paras)
	/* Reunoilta ei löytynyt puhdasta sijaintia. Silloin tarkistetaan myös keskeltä. */
	for (int k=0; k<qoh_legend_ntries; k++)
	    for (int j=0; j<nrows_arr[k]; j++)
		for (int i=0; i<ncols_arr[k]; i++)
		    if (laskenta[k][j][i] < paras) {
			paras_sij[current_zeros][0] = sisä_e;
			paras_sij[current_zeros][1] = j;
			paras_sij[current_zeros][2] = i;
			paras_sij[current_zeros][3] = k;
			paras = laskenta[k][j][i];
			if (paras == 0 && ++current_zeros >= max_zeros)
			    goto valmis;
		    }

valmis:
    int ibest = 0, izero = 0, corner_thing[4] = {0};
    /* Situation close to the corners. */
    for (; izero<current_zeros && paras_sij[izero][0] == nurkka_e; izero++) {
	int ix = !!paras_sij[izero][2];
	int iy = !!paras_sij[izero][1];
	switch ((int)!!paras_sij[izero][1]) {
	    case 0:
		for (int j=0; j<nrows; j++) {
		    int a = ylaskenta[ix][j];
		    if (a) break;
		    else   corner_thing[izero]++;
		}
		break;
	    case 1:
		for (int j=0; j<nrows; j++) {
		    int a = ylaskenta[ix][nrows-1-j];
		    if (a) break;
		    else   corner_thing[izero]++;
		}
		break;
	    default:
		__builtin_unreachable();
	}
	switch ((int)!!paras_sij[izero][2]) {
	    case 0:
		for (int i=0; i<ncols; i++) {
		    int a = xlaskenta[iy][i];
		    if (a) break;
		    else   corner_thing[izero]++;
		}
		break;
	    case 1:
		for (int i=0; i<ncols; i++) {
		    int a = xlaskenta[iy][ncols-1-i];
		    if (a) break;
		    else   corner_thing[izero]++;
		}
		break;
	    default:
		__builtin_unreachable();
	}
    }

    /* If corners were handled by the method above. */
    if (izero) {
	ibest = 0;
	int max = corner_thing[ibest];
	for (int i=1; i<izero; i++)
	    if (corner_thing[i] > max) {
		max = corner_thing[i];
		ibest = i;
	    }
    }
    else
	ibest = current_zeros / 2;


    int y_pikseli, x_pikseli;
    switch (paras_sij[ibest][0]) {
	case nurkka_e:
	    y_pikseli = !!paras_sij[ibest][1] * (axish - legh);
	    x_pikseli = !!paras_sij[ibest][2] * (axisw - legw);
	    break;
	case pysty_e:
	    x_pikseli = !!paras_sij[ibest][1] * (axisw - legw);
	    y_pikseli = Ysiirto(paras_sij[ibest][3]) + legh * paras_sij[ibest][2];
	    break;
	case vaaka_e:
	    y_pikseli = !!paras_sij[ibest][1] * (axish - legh);
	    x_pikseli = Xsiirto(paras_sij[ibest][3]) + legw * paras_sij[ibest][2];
	    break;
	case sisä_e:
	    y_pikseli = Ysiirto(paras_sij[ibest][3]) + legh * paras_sij[ibest][1];
	    x_pikseli = Xsiirto(paras_sij[ibest][3]) + legw * paras_sij[ibest][2];
	    break;
	default:
	    __builtin_unreachable();
    }

    y_pikseli -= qoh_legend_addh/2;
    x_pikseli -= qoh_legend_addw/2;
    legw -= qoh_legend_addw;
    legh -= qoh_legend_addh;

    if (qoh_verbose) {
	printf("paras = %i, %s, sija (y, x) = (%i, %i), kierros = %i\n", paras,
		paras_sij[ibest][0]==nurkka_e? "nurkka": paras_sij[ibest][0]==pysty_e? "pysty": paras_sij[ibest][0]==vaaka_e? "vaaka" : "sisä",
		paras_sij[ibest][1], paras_sij[ibest][2], paras_sij[ibest][3]);
	printf("nurkat:\n\t%6i %6i\n\t%6i %6i\n", nurkkalaskenta[0][0], nurkkalaskenta[0][1], nurkkalaskenta[1][0], nurkkalaskenta[1][1]);
    }

    float yosuus = (float)y_pikseli / axish;
    float xosuus = (float)x_pikseli / axisw;
    QRectF rf = {xosuus, yosuus, (float)legw/axisw, (float)legh/axish};
    ikk.plot->axisRect()->insetLayout()->setInsetPlacement(0, QCPLayoutInset::ipFree);
    ikk.plot->axisRect()->insetLayout()->setInsetRect(0, rf);
    return paras;
}

static void _legend_putsaa() {
    int n = ikk.plot->legend->itemCount();
    int ind = 0;
    for(int i=0; i<n; i++)
	if(!ikk.plot->legend->item(ind)->visible())
	    ikk.plot->legend->removeItem(ind);
	else
	    ind++;
}

static void tilaa_legendille() {
    ikk.plot->setViewport(QRect(0, 0, qoh_png_w, qoh_png_h));
    ikk.plot->replot();
    int axish = ikk.plot->axisRect()->height();
    QRect rect = ikk.plot->legend->outerRect();

    /* y2-akseli? */
    double vanha_datan_väli = ikk.y[0].size();
    double uusi_datan_väli = vanha_datan_väli / (1 - (double)rect.height() / axish); // johdettu kynällä ja paperilla
    ikk.y[0].upper = ikk.y[0].lower + uusi_datan_väli;
    tee_ymarginaali();

    if (ikk.plot->yAxis2->visible()) {
	int oli = qoh_y2;
	qoh_y2 = 1;
	double vanha_datan_väli = ikk.y[1].size();
	double uusi_datan_väli = vanha_datan_väli / (1 - (double)rect.height() / axish); // johdettu kynällä ja paperilla
	ikk.y[1].upper = ikk.y[1].lower + uusi_datan_väli;
	tee_ymarginaali();
	qoh_y2 = oli;
    }

    //ikk.gca_y()->setRange(ikk.y[0]);
    qoh_legend();
}

void _qoh_legend(unsigned sij) {
    if (!ikk.plot->legend->itemCount())
	return;
    ikk.plot->legend->setVisible(1);
    ikk.plot->legend->setFont(ikk.font);
    ikk.plot->legend->setRowSpacing(0);
    ikk.plot->legend->setColumnSpacing(0);
    _legend_putsaa();
    int qsij = 0;
    if ((int)sij == -1)
	return tilaa_legendille();
    for (int i=1; i<5; i++)
	if ((sij>>i) % 2)
	    qsij |= qoh_sij[i];
    if (!(sij & qoh_best))
	ikk.plot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::AlignmentFlag)qsij);
    else if (paras_legend()) {
	if (sij & qoh_if_fits)
	    ikk.plot->legend->setVisible(0);
	else
	    tilaa_legendille(); // TODO: hyödynnettäköön sitä reunapaikkaa, johon melkein mahtuu
    }
}

void qoh_legend_xy(float xosuus, float yosuus) {
    if (!ikk.plot->legend->itemCount())
	return;
    ikk.plot->legend->setVisible(1);
    ikk.plot->legend->setFont(ikk.font);
    ikk.plot->legend->setRowSpacing(0);
    ikk.plot->legend->setColumnSpacing(0);
    _legend_putsaa();

    QRect rect = ikk.plot->legend->outerRect();
    int _x, _y, legw, legh;
    int axisw = ikk.plot->axisRect()->width();
    int axish = ikk.plot->axisRect()->height();
    rect.getRect(&_x,&_y,&legw,&legh);
    QRectF rf = {xosuus, yosuus, (float)legw/axisw, (float)legh/axish};
    ikk.plot->axisRect()->insetLayout()->setInsetPlacement(0, QCPLayoutInset::ipFree);
    ikk.plot->axisRect()->insetLayout()->setInsetRect(0, rf);
}

void qoh_legend_ncols(int ncols) {
    _legend_putsaa();
    int n = ikk.plot->legend->itemCount();
    int nrows = n / ncols + !!(n % ncols);
    ikk.plot->legend->setWrap(nrows);
    ikk.plot->legend->setFillOrder(QCPLegend::foRowsFirst);
    // ikk.plot->legend->simplify();
}

void qoh_show() {
    ikk.plot->show();
    app.exec();
}

void qoh_savefig(const char* nimi) {
    int len = strlen(nimi);
    int pdf = *(int*)".pdf";
    int jpg = *(int*)".jpg";
    int __attribute__((may_alias)) ending = 0;
    if (len >= 4)
	ending = *(int*)(nimi+len-4);
    if (ending == pdf)
	ikk.plot->savePdf(nimi, qoh_png_w, qoh_png_h);
    else if (ending == jpg)
	ikk.plot->saveJpg(nimi, qoh_png_w, qoh_png_h);
    else
	ikk.plot->savePng(nimi, qoh_png_w, qoh_png_h, qoh_png_scale, qoh_png_laatu);
}

/*------------------------------------------------------*/
#if 0
QCPColorMap* kuva;

qoh_alue_t qoh_koordalue; // mitkä ovat kuvan nurkkien koordinaatit
unsigned char qoh_interpoloi; // tähän tosi, jos kuva halutaan interpoloida

enum qoh_nh_e qoh_nh;         // tähän voi vaihtaa qoh_nh = qoh_nh_none, jos epälukuja ei ole
enum QCPColorGradient::NanHandling qoh_nhlista[] = {
    [qoh_nh_transparent] = QCPColorGradient::nhTransparent,
    [qoh_nh_none]        = QCPColorGradient::nhNone,
};

void _qoh_colormesh(int tyyppi, const void* dt, unsigned ypit, unsigned xpit) {
    kuva = new QCPColorMap(ikk.plot->xAxis, ikk.plot->yAxis);
    kuva->data()->setSize(xpit, ypit);
    _qoh_täytä_cmapdata(tyyppi, dt, ypit, xpit, kuva->data());
    kuva->rescaleDataRange(1);
    QCPColorGradient grad(QCPColorGradient::gpThermal);
    grad.setNanHandling(qoh_nhlista[qoh_nh]);
    kuva->setGradient(grad);
    kuva->setInterpolate(qoh_interpoloi);
    ikk.plot->rescaleAxes();
    ikk.plot->replot();
}

void Ikkuna::paintEvent(QPaintEvent* _) {
    auto lista = ikk.plot->xAxis->plottables();
    QCPColorMap* kuva = (QCPColorMap*)lista.at(lista.size()-1);
    int xpit = kuva->data()->keySize(),
	ypit = kuva->data()->valueSize();
    // TODO: muut kuin lat-lon-koordinaatistot
    QCPPainter piirtäjä(this);
    piirtäjä.setPen(Qt::SolidLine);
    //piirtäjä.setPen(QPen(QColor(255,0,0)));
    piirtäjä.drawLine(QLineF(1,2,20,30));
    piirtäjä.drawLine(QLineF(1,2,200,300));
    piirtäjä.drawLine(QLineF(0,2.41,0,2.41));
    piirtäjä.drawLine(QLineF(0,0,0.5,0.5));
    piirtäjä.drawLine(QLineF(2,2,4,4));
    ikk.plot->toPainter(&piirtäjä);
    ikk.plot->replot();
    puts("hep");
}

void qoh_coastlines() {
    ikk.plot->setAttribute(Qt::WA_PaintOnScreen);
    ikk.plot->update();
}
#endif

#undef _min
#undef _max
