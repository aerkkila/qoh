#ifndef __qoh_h__
#define __qoh_h__

#if !defined __qoh_internal__ && !defined QOH_NO_CMH
#define CMH_ENUM_ONLY
#include <cmh_colormaps.h> // to allow using the colormap enumerator, cmh_cmapname_e
#undef CMH_ENUM_ONLY
#endif

#define qoh_i1 11
#define qoh_i2 12
#define qoh_i4 14
#define qoh_i8 18
#define qoh_u1 1
#define qoh_u2 2
#define qoh_u4 4
#define qoh_u8 8
#define qoh_f4 24
#define qoh_f8 28
#define qoh_f10 36 // Usein f10 vie tilaa 16 tavua (ks. padding bytes), mutta voi myös vaihdella.

/* Palauttaa oikean datatyypin mukaan jonkin yllä määritellyistä luvista, esim. typeof(a) = float32 -> qoh_f4 */
#define qoh_tyyppi(a) ((int)( \
	    (typeof(a))1.5 == 1 ?	/* kokonais- vai liukuluku */ \
	    /* kokonaisluku */ \
	    sizeof(a) + (		/* kokonaisluvun koko */ \
	    (typeof(a))(-1) < 0 ?	/* merkillinen vai merkitön kokonaisluku */ \
	     10 : 0 )			/* merkillisen tunniste on koko + 10 */ \
	    /* liukuluku */ \
	    : sizeof(a) + 20 ))		/* liukuluvun tunniste on koko + 20 */

#define _qoh_left	1
#define _qoh_right	2
#define _qoh_top	3
#define _qoh_bottom	4
#define _qoh_best	5
#define _qoh_if_fits	6
#define qoh_sijainteja	7

#define qoh_left	(1<<_qoh_left)
#define qoh_right	(1<<_qoh_right)
#define qoh_top		(1<<_qoh_top)
#define qoh_bottom	(1<<_qoh_bottom)
#define qoh_best	(1<<_qoh_best)
#define qoh_if_fits	(1<<_qoh_if_fits)

#define qoh_scalemod_none 0
#define qoh_scalemod_mult 1
#define qoh_scalemod_powr 2

#define qoh_nogroup -1
#define qoh_ignoregroup -2

#ifdef __cplusplus
extern "C" {
#endif

typedef struct { unsigned char a[3]; } qoh_väri;

extern int qoh_png_w, qoh_png_h, qoh_png_laatu;
extern float qoh_fontsize, // to be changed only with qoh_set_fontsize
       qoh_xmargin;
extern double qoh_png_scale;
extern unsigned char qoh_sorted, qoh_verbose;
extern char qoh_y2, qoh_x2; // Set to true to plot using the secondary x or y axis.
extern const unsigned char qoh_oletusvärejä, qoh_värit[8][3];
extern unsigned char qoh_scattersize;
extern int qoh_legend_ntries; // The larger the value, the better the legend is placed automatically.
extern int qoh_legend_addw, qoh_legend_addh; // legendin automaattisijoituksessa vaaditaan enemmän tilaa
extern short qoh_colorgroup;

void _qoh_dashstyle(float dashlength, float space, int optional_taakse);

/* {"", "-", "--", "..", "-."} */
void _qoh_linestyle(const char*, int optional_taakse);

/* jokin näistä:  .x+Oo*< */
void _qoh_scatterstyle(int, int optional_taakse);

#if 0
struct qoh_args {
    int N;
    void *y, *x, *z;
    int ytype, xtype, ztype;
    double xstep, x0, *colornorm;
    int cmap;
};

static inline void qoh_plot_y_(int ytyyppi, void *y, struct qoh_args args) {
    args.y = y;
    args.ytype = ytyyppi;
    qoh_plot_args(&args);
}

static inline void qoh_plot_yx_(int ytyyppi, void *y, int xtyyppi, void *x, struct qoh_args args) {
    args.y = y;
    args.ytype = ytyyppi;
    args.x = x;
    args.xtype = xtyyppi;
    qoh_plot_args(&args);
}

static inline void qoh_plot_yz_(int ytyyppi, void *y, int ztyyppi, void *z, struct qoh_args args) {
    args.y = y;
    args.ytype = ytyyppi;
    args.z = z;
    args.ztype = ztyyppi;
    qoh_plot_args(&args);
}

static inline void qoh_plot_yxz_(int ytyyppi, void *y, int xtyyppi, void *x, int ztyyppi, void *z, struct qoh_args args) {
    args.y = y;
    args.ytype = ytyyppi;
    args.x = x;
    args.xtype = xtyyppi;
    args.z = z;
    args.ztype = ztyyppi;
    qoh_plot_args(&args);
}

#define qoh_plot_y_opt(x, ...) qoh_plot_y_(qoh_tyyppi(y), y, (struct qoh_args){__VA_ARGS__})
#define qoh_plot_yx_opt(y, x, ...) qoh_plot_yx_(qoh_tyyppi(y), y, qoh_tyyppi(x), x, (struct qoh_args){__VA_ARGS__})
#define qoh_plot_yz_opt(y, z, ...) qoh_plot_yz_(qoh_tyyppi(y), y, qoh_tyyppi(z), z, (struct qoh_args){__VA_ARGS__})
#define qoh_plot_yxz_opt(y, x, z, ...) qoh_plot_yxz_(qoh_tyyppi(y), y, qoh_tyyppi(x), x, qoh_tyyppi(z), z, (struct qoh_args){__VA_ARGS__})
#endif

int qoh_graphcount(); // is smaller than ngraphs if plot_yz has been used
int  qoh_ngraphs();
void qoh_clf();
void _qoh_plot_y(int automatic_tyyppi, const void* data, unsigned N, double optional_x0, double optional_xstep);
void _qoh_plot_yx(int automatic_ytyyppi, int automatic_xtyyppi, const void* datay, const void* datax, unsigned N);
/* Pisteitten värit valitaan z-datan arvojen mukaan icmap-numerolla osoitetusta värikartasta. */
/* Varoitus: ikk.plot.graphCount() kasvaa enemmän kuin yhdellä, vaikka piirretään vain yksi asia.
   Esim. linestyle yms. funktiot eivät siten toimi tämän jälkeen.
   norm on muotoa {min, center, max}. Mikä tahansa näistä voi olla NAN. */
void _qoh_plot_yz(int automatic_ytyyppi, int automatic_ztyyppi, const void* datay, const void* dataz, unsigned N, int icmap, double* optional_norm, double optional_x0, double optional_xstep);
void qoh_colorbar(const char* title, int place); // can be called after qoh_plot_yz; place = qoh_left, yms.
double qoh_get_lower(int where); // which axis: qoh_left, ...
double qoh_get_upper(int where); // which axis: qoh_left, ...
void qoh_line(double y0, double x0, double y1, double x1);
void qoh_title(const char*);
void _qoh_label(const char*, int optional_taakse);
void _qoh_legend(unsigned optional_sij);
void qoh_legend_xy(float x, float y);
void qoh_legend_ncols(int ncols);
void qoh_xlabel(const char*);
void qoh_ylabel(const char*);
void qoh_xlabel1(const char*);
void qoh_ylabel1(const char*);
void qoh_xlabel2(const char*);
void qoh_ylabel2(const char*);
void qoh_xmax(double);
void qoh_xmin(double);
void qoh_ymax(double);
void qoh_ymin(double);
void qoh_set_fontsize(float size);
void qoh_set_range(int where, double lower, double upper);
void* qoh_init_ticker(const char *axisname);
void qoh_set_tick(void *vticker, float position, const char *label);
void qoh_set_visible(int where, int true_or_false);
void qoh_set_grid_visible(int where, int true_or_false);
void qoh_xaxis_log(double base);
void qoh_yaxis_log(double base);
void _qoh_xticker_datetime(const char* format, int optional_tickcount); // current x-axis
void _qoh_yticker_datetime(const char* format, int optional_tickcount); // current y-axis
void _qoh_xticker1_datetime(const char* format, int optional_tickcount); // left axis
void _qoh_yticker1_datetime(const char* format, int optional_tickcount); // bottom axis
void _qoh_xticker2_datetime(const char* format, int optional_tickcount); // top axis
void _qoh_yticker2_datetime(const char* format, int optional_tickcount); // right axis
void _qoh_ticker_fixed(int xy, double step, int optional_scalemod); // xy voi olla 'x'+'y', 2, 'x', 0, 'y', 1
void _qoh_xticker_log(double base, int optional_tickcount);
void _qoh_yticker_log(double base, int optional_tickcount);
void qoh_ticklabels_inside();
void qoh_ticklabels_outside(); // default
void qoh_ticks_inside();
void qoh_ticks_outside(); // default
void _qoh_linewidth(float, int optional_taakse);
void qoh_show();
void qoh_savefig(const char*);
void qoh_color_rgb(unsigned char, unsigned char, unsigned char, int taakse);
void qoh_color_ptr(unsigned char *rgb, int taakse);
void qoh_set_colors_from(int cmh_enum); // colorify all graphes with a colorscale
void qoh_color_default_n(int n);

#define _1qoh_legend(sij, ...) _qoh_legend(sij)
#define qoh_legend(...) _1qoh_legend(__VA_ARGS__ __VA_OPT__(,) qoh_best)
#define _1qoh_plot_y(data, n, x0, xstep, ...) _qoh_plot_y(qoh_tyyppi((data)[0]), data, n, x0, xstep)
#define qoh_plot_y(data, ...) _1qoh_plot_y(data, __VA_ARGS__, 0, 1)
#define qoh_plot_yx(datay, datax, ...) _qoh_plot_yx(qoh_tyyppi((datay)[0]), qoh_tyyppi((datax)[0]), datay, datax, __VA_ARGS__)
#define _1qoh_plot_yz(datay, dataz, n, icmap, norm, x0, xstep, ...) _qoh_plot_yz(qoh_tyyppi((datay)[0]), qoh_tyyppi((dataz)[0]), datay, dataz, n, icmap, norm, x0, xstep)
#define qoh_plot_yz(datay, dataz, ...) _1qoh_plot_yz(datay, dataz, __VA_ARGS__, NULL, 0, 1)

#define _1qoh_label(a, b, ...) _qoh_label(a, b)
#define qoh_label(...) _1qoh_label(__VA_ARGS__, 0)
#define _1qoh_linewidth(a, b, ...) _qoh_linewidth(a, b)
#define qoh_linewidth(...) _1qoh_linewidth(__VA_ARGS__, 0)
#define _1qoh_dashstyle(a, b, c, ...) _qoh_dashstyle(a, b, c)
#define qoh_dashstyle(...) _1qoh_dashstyle(__VA_ARGS__, 0)
#define _1qoh_linestyle(a, b, ...) _qoh_linestyle(a, b)
#define qoh_linestyle(...) _1qoh_linestyle(__VA_ARGS__, 0)
#define _1qoh_scatterstyle(a, b, ...) _qoh_scatterstyle(a, b)
#define qoh_scatterstyle(...) _1qoh_scatterstyle(__VA_ARGS__, 0)
#define _1qoh_ticker_fixed(a, b, c, ...) _qoh_ticker_fixed(a, b, c)
#define qoh_ticker_fixed(...) _1qoh_ticker_fixed(__VA_ARGS__, qoh_scalemod_none)
#define _1qoh_xticker_datetime(muoto, määrä, ...) _qoh_xticker_datetime(muoto, määrä)
#define qoh_xticker_datetime(...) _1qoh_xticker_datetime(__VA_ARGS__, -1)
#define _1qoh_xticker2_datetime(muoto, määrä, ...) _qoh_xticker2_datetime(muoto, määrä)
#define qoh_xticker2_datetime(...) _1qoh_xticker2_datetime(__VA_ARGS__, -1)
#define _1qoh_yticker_datetime(muoto, määrä, ...) _qoh_yticker_datetime(muoto, määrä)
#define qoh_yticker_datetime(...) _1qoh_yticker_datetime(__VA_ARGS__, -1)
#define _1qoh_yticker2_datetime(muoto, määrä, ...) _qoh_yticker2_datetime(muoto, määrä)
#define qoh_yticker2_datetime(...) _1qoh_yticker2_datetime(__VA_ARGS__, -1)
#define _1qoh_xticker_log(kanta, määrä, ...) _qoh_xticker_log(kanta, määrä)
#define qoh_xticker_log(...) _1qoh_xticker_log(__VA_ARGS__, -1)
#define _1qoh_yticker_log(kanta, määrä, ...) _qoh_yticker_log(kanta, määrä)
#define qoh_yticker_log(...) _1qoh_yticker_log(__VA_ARGS__, -1)
/*-----------------------------------------------------------*/
#if 0
typedef struct { float x0, x1, y0, y1; } qoh_alue_t;
extern qoh_alue_t qoh_koordalue;
extern unsigned char qoh_interpoloi;
enum qoh_nh_e {qoh_nh_transparent, qoh_nh_none};
extern enum qoh_nh_e qoh_nh;
#define qoh_colormesh(a, ...) _qoh_colormesh(qoh_tyyppi(a[0]), a, __VA_ARGS__)
void _qoh_colormesh(int tyyppi, const void* dt, unsigned xpit, unsigned ypit);
void qoh_coastlines();
#endif

#ifdef __cplusplus
}
#endif

#endif
