/* Edited from qcustomplot source code.
   Here negative numbers work fine unlike in qcustomplot.
   TODO base e. */

struct QohAxisTickerLog : public QCPAxisTicker
{
    QohAxisTickerLog();

    // getters:
    double logBase() const { return mLogBase; }
    int subTickCount() const { return mSubTickCount; }

    // setters:
    void setLogBase(double base);
    void setTickCount(int subTicks); // affects also subtickcount
    void setSubTickCount(int subTicks);

    // property members:
    double mLogBase;
    int mSubTickCount;
    double epsilon; // No ticks in range from -ε to ε

    // non-property members:
    double mLogBaseLnInv;

    // reimplemented virtual methods:
    virtual int getSubTickCount(double tickStep) Q_DECL_OVERRIDE;
    virtual QVector<double> createTickVector(double tickStep, const QCPRange &range) Q_DECL_OVERRIDE;
};

QohAxisTickerLog::QohAxisTickerLog() :
    mLogBase(10.0),
    mSubTickCount(8), // generates 10 intervals
    epsilon(1e-10),
    mLogBaseLnInv(1.0/qLn(mLogBase))
{ }

void QohAxisTickerLog::setLogBase(double base) {
    if (base < 0)
	qDebug() << Q_FUNC_INFO << "log base has to be greater than zero:" << base;
    mLogBase = base;
    mLogBaseLnInv = 1.0/qLn(mLogBase);
}

void QohAxisTickerLog::setSubTickCount(int subTicks) {
    if (subTicks < 0)
	qDebug() << Q_FUNC_INFO << "sub tick count can't be negative:" << subTicks;
    mSubTickCount = subTicks;
}

void QohAxisTickerLog::setTickCount(int count) {
    mTickCount = count;
    mSubTickCount = count - 2;
}

int QohAxisTickerLog::getSubTickCount(double tickStep) {
    return mSubTickCount;
}

/*! \internal
  
  Creates ticks with a spacing given by the logarithm base and an increasing integer power in the
  provided \a range. The step in which the power increases tick by tick is chosen in order to keep
  the total number of ticks as close as possible to the tick count (\ref setTickCount).

  The parameter \a tickStep is ignored for the normal logarithmic ticker generation. Only when
  zoomed in very far such that not enough logarithmically placed ticks would be visible, this
  function falls back to the regular QCPAxisTicker::createTickVector, which then uses \a tickStep.
  
  \seebaseclassmethod
*/
QVector<double> QohAxisTickerLog::createTickVector(double tickStep, const QCPRange &range) {
    QVector<double> result;
    if (range.lower < 0 && range.upper > 0) {
	QCPRange negative(range.lower, -this->epsilon);
	QCPRange positive(this->epsilon, range.upper);
	int tc = this->mTickCount;
	this->mTickCount = tc/2;
	QVector result = this->createTickVector(tickStep, negative);
	this->mTickCount = tc - this->mTickCount;
	result.append(this->createTickVector(tickStep, positive));
	this->mTickCount = tc;
	return result;
    }

    if (range.lower > 0) {
	const double baseTickCount = qLn(range.upper/range.lower)*mLogBaseLnInv;
	if (baseTickCount < 1.6) // if too few log ticks would be visible in axis range, fall back to regular tick vector generation
	    return QCPAxisTicker::createTickVector(tickStep, range);
	const double exactPowerStep = baseTickCount/double(mTickCount+1e-10);
	const double newLogBase = qPow(mLogBase, qMax(int(cleanMantissa(exactPowerStep)), 1));
	double currentTick = qPow(newLogBase, qFloor(qLn(range.lower)/qLn(newLogBase)));
	result.append(currentTick);
	while (currentTick < range.upper && currentTick > 0) // currentMag might be zero for ranges ~1e-300, just cancel in that case
	{
	    currentTick *= newLogBase;
	    result.append(currentTick);
	}
	return result;
    }

    /* negative */
    const double baseTickCount = qLn(range.lower/range.upper)*mLogBaseLnInv;
    if (baseTickCount < 1.6) // if too few log ticks would be visible in axis range, fall back to regular tick vector generation
	return QCPAxisTicker::createTickVector(tickStep, range);
    const double exactPowerStep =  baseTickCount/double(mTickCount+1e-10);
    const double newLogBase = qPow(mLogBase, qMax(int(cleanMantissa(exactPowerStep)), 1));
    double currentTick = -qPow(newLogBase, qCeil(qLn(-range.lower)/qLn(newLogBase)));
    result.append(currentTick);
    while (currentTick < range.upper && currentTick < 0) // currentMag might be zero for ranges ~1e-300, just cancel in that case
    {
	currentTick /= newLogBase;
	result.append(currentTick);
    }
    return result;
}
