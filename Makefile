include config.mk

ifdef have_colormap_headers
	CFLAGS += -DCOLORMAP_HEADERS
endif

libs = `pkg-config --libs Qt5Widgets` -lqcustomplot

all: libqoh.so

libqoh.so: qoh.cc qoh.h liitettävää.h config.mk
	$(C++) $(CFLAGS) -fpic -shared -fpermissive -o $@ $< `pkg-config --cflags Qt5Widgets` $(libs)

install: libqoh.so
	cp libqoh.so $(prefix)/lib
	cp qoh.h $(prefix)/include

uninstall:
	rm $(prefix)/lib/libqoh.so $(prefix)/include/qoh.h

clean:
	rm -f *.so *.out
