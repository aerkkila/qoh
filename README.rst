==============================================
Qoh: A plotting library in C using QCustomPlot
==============================================

Warning
-------
This is a temporary library.

Features
--------
In addition to some features of QCustomPlot, I have implemented new features:
    - Finding the best location for legend
    - plot_yz: besides x and y vector, this functions takes z vector which will be described with colors of the dots.

A pointer to any common variable type (char*, short*, unsigned short*, double*, ...) can be given to the plot functions.
The data type will be recognized using some macro hacks.

Many functions also have optional arguments which is possible through some macro hacks as well.

Installation
------------
If needed, edit configuration config.mk, then:

    | make
    | make install

Dependencies:
    * QCustomPlot

Optional dependencies, see config.mk to disable these:
    * colormap-headers (https://codeberg.org/aerkkila/colormap-headers)

Usage
-----
| When the header defines the following function:
| _qoh_foo(int automatic_type, void* vector, float b, int optional_c),
| there is also a macro to be used without the underscore:
| qoh_foo(vector, b, c);
| Here c is an optional argument and can be omitted.

Also the automatic_type parameter describing the data type of vector[0] will be automatically added by the macros.
