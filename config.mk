# colormap-headers (codeberg.com/aerkkila/colormap-headers) defines several colormaps.
# Comment this out, if you don't want that. Then only a grayscale colormap is available.
# Then also, programs with '#include <qoh.h>' must be compiled with -DQOH_NO_CMH
have_colormap_headers = 1

prefix = /usr/local
C++ = g++
CFLAGS = -g -Wall -O2
